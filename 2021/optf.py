from os import system
from glob import glob
import subprocess

system('g++ parameterized_greedy.cpp -o parameterized_greedy -std=c++17 -O2')
#for f in sorted(glob('*.txt')):
#  print(f)
#  system('./parameterized_greedy < %s > %s' % (f, f[:-4] + '.out'))
#  system('./simulator %s %s' % (f, f[:-4] + '.out'))
f = "f.txt"
max_out = 0
for t in range(140, 180, 5):
    for weight in range(220, 340, 10):
        print('./parameterized_greedy %d %d < %s > %s' % (weight, t, f, f[:-4] + '.out'))
        system('./parameterized_greedy %d %d < %s > %s' % (weight, t, f, f[:-4] + '.out'))
        out = subprocess.check_output('./simulator %s %s' % (f, f[:-4] + '.out'), shell=True)
        print(int(out))
        out = int(out)
        if out > max_out:
            max_out = out
            best = (weight, t)
print("Best ", max_out)
print(best)
