#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <ctime>
#include <cmath>
#include <string>
#include <iomanip>
#include <vector>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <cassert>
#include <queue>
#include <bitset>

using namespace std;

#define clr(a) memset(a, 0, sizeof(a))
#define pb push_back
#define forab(i, a, b) for(int i = int(a); i < int(b); ++i)
#define forba(i, b, a) for(int i = int(b) - 1; i >= int(a); --i)
#define forn(i, n) forab(i, 0, n)

typedef long long ll;
typedef long double ld;

const int INF = 1e9;
const int MAXN = 2e5 + 1000;
const int MAXM = 2e5 + 1000;
const int MAXV = 2100;

struct edge {
   int st, en;
   string name;
   int length;
   int index;
};

struct car {
    int length;
    vector<edge*> path;
    int index;
};

edge edges[MAXM];
car cars[MAXV];
int edge_appears_in_paths[MAXM];
vector<edge*> edges_in[MAXN];
vector<edge*> edges_out[MAXN];
map<string, edge*> from_street_name;

vector<pair<int, string>> out[MAXN];

int main() {
#ifdef LOCAL
    freopen("a.txt", "r", stdin);
    //freopen("a.out", "w", stdout);
    //freopen("", "w", stderr);
#endif
    cin.sync_with_stdio(false);
    cin.tie(0);
    int D, I, S, V, F;
    cin >> D >> I >> S >> V >> F;

    forn(i, S) {
        edges[i].index = i;
        cin >> edges[i].st >> edges[i].en >> edges[i].name >> edges[i].length;
        from_street_name[edges[i].name] = &edges[i];
        edges_in[edges[i].en].push_back(&edges[i]);
        edges_out[edges[i].st].push_back(&edges[i]);
    }

    forn(i, V) {
        cars[i].index = i;
        cin >> cars[i].length;
        forn(j, cars[i].length) {
            string name;
            cin >> name;
            auto e = from_street_name[name];
            cars[i].path.push_back(e);
            edge_appears_in_paths[e->index]++;
        }
    }


    vector<int> non_empty_schedules;
    forn(i, I) {
        int sum = 0;
        int min_weight = INF;
        vector<pair<int, string>> streets;
        for (auto e : edges_in[i]) {
            sum += edge_appears_in_paths[e->index];
            if (edge_appears_in_paths[e->index] > 0) {
                streets.push_back({edge_appears_in_paths[e->index], e->name});
                min_weight = min(min_weight, edge_appears_in_paths[e->index]);
            }
        }
        //sort(streets.begin(), streets.end());
        
        sum /= min_weight;
        for (auto &x : streets) {
            x.first /= min_weight;
        }
        int target_sum = 2 * streets.size();
        for (auto &x : streets) {
            x.first = max(1, int(floor(x.first * 1.0 / sum * target_sum)));
        }
        min_weight = INF;
        for (auto x : streets) {
            min_weight = min(min_weight, x.first);
        }
        for (auto &x : streets) {
            x.first /= min_weight;
        }
        
        out[i] = streets;

        if (out[i].size() > 0) {
            non_empty_schedules.push_back(i);
        }
    }
    cout << non_empty_schedules.size() << '\n';
    for (int i : non_empty_schedules) {
        cout << i << '\n' << out[i].size() << '\n';
        for (auto x : out[i]) {
            cout << x.second << ' ' << x.first << '\n';
        }
    }
    
    return 0;
}

