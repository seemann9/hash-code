from os import system
from glob import glob

system('g++ solver.cpp -o solver -std=c++17 -O2')
for f in glob('*.txt'):
  system('./solver < %s > %s' % (f, f[:-4] + '.out'))
