#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <ctime>
#include <cmath>
#include <string>
#include <iomanip>
#include <vector>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <cassert>
#include <queue>
#include <bitset>

using namespace std;

#define clr(a) memset(a, 0, sizeof(a))
#define pb push_back
#define forab(i, a, b) for(int i = int(a); i < int(b); ++i)
#define forba(i, b, a) for(int i = int(b) - 1; i >= int(a); --i)
#define forn(i, n) forab(i, 0, n)

typedef long long ll;
typedef long double ld;

const int INF = 1e9;
const int MAXN = 2e5 + 1000;
const int MAXM = 2e5 + 1000;
const int MAXV = 2100;

struct edge {
   int st, en;
   string name;
   int length;
   int index;
};

struct car {
    int length;
    vector<edge*> path;
    int index;
};

edge edges[MAXM];
car cars[MAXV];
int edge_appears_in_paths[MAXM];
vector<edge*> edges_in[MAXN];
vector<edge*> edges_out[MAXN];
map<string, edge*> from_street_name;

vector<pair<string, int>> out[MAXN];

int main() {
#ifdef LOCAL
    freopen("a.txt", "r", stdin);
    //freopen("a.out", "w", stdout);
    //freopen("", "w", stderr);
#endif
    cin.sync_with_stdio(false);
    cin.tie(0);
    int D, I, S, V, F;
    cin >> D >> I >> S >> V >> F;
    int cycles;
    if (D == 6) {
        cycles = 3;
    } else {
        cycles = 20;
    }

    forn(i, S) {
        edges[i].index = i;
        cin >> edges[i].st >> edges[i].en >> edges[i].name >> edges[i].length;
        from_street_name[edges[i].name] = &edges[i];
        edges_in[edges[i].en].push_back(&edges[i]);
        edges_out[edges[i].st].push_back(&edges[i]);
    }

    forn(i, V) {
        cars[i].index = i;
        cin >> cars[i].length;
        forn(j, cars[i].length) {
            string name;
            cin >> name;
            auto e = from_street_name[name];
            cars[i].path.push_back(e);
            edge_appears_in_paths[e->index]++;
        }
    }


    vector<int> non_empty_schedules;
    forn(i, I) {
        int sum = 0;
        for (auto e : edges_in[i]) {
            sum += edge_appears_in_paths[e->index];
        }
        
        for (auto e : edges_in[i]) {
            int duration = floor(edge_appears_in_paths[e->index] * 1.0 / sum * (D / cycles));
            if (duration > 0) {
                out[i].push_back({e->name, duration});
            }
        }
        if (out[i].size() > 0) {
            non_empty_schedules.push_back(i);
        }
    }
    cout << non_empty_schedules.size() << '\n';
    for (int i : non_empty_schedules) {
        cout << i << '\n' << out[i].size() << '\n';
        for (auto x : out[i]) {
            cout << x.first << ' ' << x.second << '\n';
        }
    }
    
    return 0;
}

