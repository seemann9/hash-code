#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <ctime>
#include <cmath>
#include <string>
#include <iomanip>
#include <vector>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <cassert>
#include <queue>
#include <bitset>

using namespace std;

#define clr(a) memset(a, 0, sizeof(a))
#define pb push_back
#define forab(i, a, b) for(int i = int(a); i < int(b); ++i)
#define forba(i, b, a) for(int i = int(b) - 1; i >= int(a); --i)
#define forn(i, n) forab(i, 0, n)

typedef long long ll;
typedef long double ld;

const int INF = 1e9;
const int MAXN = 2e5 + 1000;
const int MAXM = 2e5 + 1000;
const int MAXV = 2100;

struct edge {
   int st, en;
   string name;
   int length;
};

struct car {
    int length;
    vector<edge*> path;
};

edge edges[MAXM];
car cars[MAXV];
vector<edge*> edges_in[MAXN];
vector<edge*> edges_out[MAXN];
map<string, edge*> from_street_name;

int main() {
#ifdef LOCAL
    freopen("a.txt", "r", stdin);
    freopen("a.out", "w", stdout);
    //freopen("", "w", stderr);
#endif
    cin.sync_with_stdio(false);
    cin.tie(0);
    int D, I, S, V, F;
    cin >> D >> I >> S >> V >> F;
    forn(i, S) {
        cin >> edges[i].st >> edges[i].en >> edges[i].name >> edges[i].length;
        from_street_name[edges[i].name] = &edges[i];
        edges_in[edges[i].en].push_back(&edges[i]);
        edges_out[edges[i].st].push_back(&edges[i]);
    }

    forn(i, V) {
        cin >> cars[i].length;
        forn(j, cars[i].length) {
            string name;
            cin >> name;
            cars[i].path.push_back(from_street_name[name]);
        }
    }


    cout << I << '\n';
    forn(i, I) {
        cout << i << '\n' << edges_in[i].size() << '\n';
        for (auto e : edges_in[i]) {
            cout << e->name << ' ' << 1 << '\n';
        }
    }
    
    return 0;
}

