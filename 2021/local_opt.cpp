#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <ctime>
#include <cmath>
#include <string>
#include <iomanip>
#include <vector>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <cassert>
#include <queue>
#include <bitset>

using namespace std;

#define forn(i, n) for (int i = 0; i < int(n); ++i)
#define forab(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ford(i, n) for (int i = int(n) - 1; i >= 0; --i)
#define sz(c) int((c).size())
#define all(c) (c).begin(), (c).end()
#define mp(x, y) make_pair(x, y)
#define pb push_back
#define fst first
#define snd second

using ll = long long;
using vi = vector<int>;
using vll = vector<ll>;
using pii = pair<int, int>;
using vvi = vector<vi>;

#ifdef LOCAL
#define eprintf(args...) fprintf(stderr, args), fflush(stderr);
#else
#define eprintf(args...) ;
#endif

#define FILE_NAME "a"

int D, I, S, V, F;
    
const int INF = 1e9;
const int MAXN = 2e5 + 1000;
const int MAXM = 2e5 + 1000;
const int MAXV = 2100;

struct edge {
   int id, st, en;
   string name;
   int length;
   int on, off, mod;

   int out_id, out_pos;
};

struct car {
    int length;
    vector<edge*> path;
    int index, position;
};

edge edges[MAXM];
car cars[MAXV];
vector<edge*> edges_in[MAXN];
vector<edge*> edges_out[MAXN];
map<string, edge*> from_street_name;
// vector<vector<pair<edge*, int>>> duration;
// vector<int> sum_duration;
vector<queue<int>> cars_in_end;
int used_street[MAXN];
int added_to_queue[MAXN];
int max_queue_size[MAXN];

void read_input(char* fname) {
	freopen(fname, "r", stdin);

	cin >> D >> I >> S >> V >> F;
	// cout << D << ' ' << I << ' ' << S << ' ' << V << ' ' << F << endl;
	
    forn(i, S) {
        cin >> edges[i].st >> edges[i].en >> edges[i].name >> edges[i].length;
        edges[i].id = i;
        edges[i].mod = 0;
        from_street_name[edges[i].name] = &edges[i];
        edges_in[edges[i].en].push_back(&edges[i]);
        edges_out[edges[i].st].push_back(&edges[i]);
    }

    forn(i, V) {
        cin >> cars[i].length;
        forn(j, cars[i].length) {
            string name;
            cin >> name;
            cars[i].path.push_back(from_street_name[name]);
        }
    }

    fclose(stdin);
}

int A;
int II[MAXN];
int E[MAXN];
vector<pair<edge*, int>> duration[MAXN];
int sum[MAXN];



void read_output(char* fname) {
	freopen(fname, "r", stdin);

	cin >> A;

	// int D, I, S, V, F;
	//  cin >> D >> I >> S >> V >> F;
	
	forn(i, A) {
    	cin >> II[i];
    	cin >> E[i];
    	
    	sum[i] = 0;
    	duration[i].clear();

    	forn(j, E[i]) {
    		string S;
    		int d;
    		cin >> S >> d;
    		duration[i].push_back(make_pair(from_street_name[S], d));
    		sum[i] += d;
    	}

    	int cur_d = 0;

    	for (int j = 0; j < duration[i].size(); ++j) {
    		auto p = duration[i][j];
    		auto e = p.fst;
    		int d = p.snd;

    		e->out_id = i;
    		e->out_pos = j;

    		e->on = cur_d;
    		e->off = cur_d + d;
    		e->mod = sum[i];

    		cur_d += d;
    	}
    }

    // forn(i, I) {
    // 	cout << sum_duration[i] << endl;
    // 	for (auto e: duration[i]) {
    // 		cout << e.fst->st << ' ' << e.snd << endl;
    // 	}
    // }

    fclose(stdin);
}

vector<pair<edge*, pair<int, int> > > changes;

void update_ans(edge *e) {
	int i = e->out_id, q = e->out_pos;

	changes.pb(mp(e, mp(e->off - e->on, duration[i][q].snd)));

	sum[i] += (e->off - e->on) - duration[i][q].snd;
	duration[i][q].snd = e->off - e->on;

	int cur_d = 0;
	for (int j = 0; j < duration[i].size(); ++j) {
		auto p = duration[i][j];
		auto e = p.fst;
		int d = p.snd;

		e->out_id = i;
		e->out_pos = j;

		e->on = cur_d;
		e->off = cur_d + d;
		e->mod = sum[i];

		cur_d += d;
	}
}

void undo_ans() {
	assert(changes.size() > 0);
	auto ch = *changes.rbegin();
	edge *e = ch.fst;
	int newdur = ch.snd.fst, olddur = ch.snd.snd;

	int i = e->out_id, q = e->out_pos;

	changes.pop_back();

	sum[i] += olddur - newdur;
	duration[i][q].snd = olddur;
	e->off = e->on + olddur;

	int cur_d = 0;
	for (int j = 0; j < duration[i].size(); ++j) {
		auto p = duration[i][j];
		auto e = p.fst;
		int d = p.snd;

		e->out_id = i;
		e->out_pos = j;

		e->on = cur_d;
		e->off = cur_d + d;
		e->mod = sum[i];

		cur_d += d;
	}
}

void print_ans(char* fname) {
	freopen(fname, "w", stdout);

	cout << A << '\n';

	// int D, I, S, V, F;
	//  cin >> D >> I >> S >> V >> F;
	
	forn(i, A) {
		cout << II[i] << '\n' << E[i] << '\n';

    	for (auto p: duration[i]) {
    		cout << p.fst->name << ' ' << p.snd << '\n';
    	}
    }

    fclose(stdout);
}

char *ans_file;

int simulate() {
	int ans = 0;

	cars_in_end.clear();
	cars_in_end.resize(S);

	// make_queues
	forn(i, V) {
		cars[i].index = 0;
		cars[i].position = cars[i].path[0]->length;
		cars_in_end[cars[i].path[0]->id].push(i);
		added_to_queue[i] = 1;
	}

	// forn(i, V) {
	// 	cout << cars[i].index << ' ' << cars[i].position << endl;
	// }

	forn(i, S) {
		used_street[i] = 0;
		max_queue_size[i] = 0;
	}

	int success = 0;

	for (int t = 1; t <= D; t++) { // simulate per second
		// cout << "SEC " << t << endl;

		forn(j, V) { // simulate per car
			// tl colors??
			if (cars[j].index == -1) {
				continue;
			}
			assert(0 <= cars[j].index && cars[j].index < cars[j].path.size());

			
			// cout << j << ' ' << eid << endl;

			if (cars[j].position != cars[j].path[cars[j].index]->length) {
				cars[j].position += 1;
			} else {
				edge* e = cars[j].path[cars[j].index];

				int eid = cars[j].path[cars[j].index]->id;
				assert(!cars_in_end[eid].empty());
				if (used_street[eid] != t && cars_in_end[eid].front() == j \
					&& e->mod != 0 && e->on <= (t - 1) % e->mod && (t - 1) % e->mod < e->off) {
					cars_in_end[eid].pop();
					used_street[eid] = t;
					added_to_queue[j] = 0;
					assert(cars[j].index + 1 != cars[j].path.size());

					cars[j].index += 1;
					cars[j].position = 1;
				}
			}

			if (cars[j].position == cars[j].path[cars[j].index]->length) { // in the end of street
				if (cars[j].index + 1 < cars[j].path.size()) { 
					if (!added_to_queue[j]) {
						int tmpeid = cars[j].path[cars[j].index]->id;
						cars_in_end[tmpeid].push(j);
						max_queue_size[tmpeid] = max(max_queue_size[tmpeid], (int) cars_in_end[tmpeid].size());
						added_to_queue[j] = 1;
					}
				} else {
					ans += F + (D - t);
					cars[j].index = -1;
					cars[j].position = -1;
					success += 1;
				}	
			}
		}

		// cout << endl;
		// forn(j, V) {
		// 	cout << j << " on " << cars[j].index << ' ' << cars[j].position << endl;
		// }
	}

	// cout << success << endl;
	// forn(i, V) {
	// 	int ok_index = cars[i].index == -1 ? 0 : cars[i].index;
	// 	cout << cars[i].index << ' ' << cars[i].path[ok_index]->name << ' ' << cars[i].path.size() << ' ' << \
	// 	cars[i].position << ' ' << cars[i].path[ok_index]->length << endl;
	// }

	// edge* e = from_street_name["dh-bcda"];
	// while (!cars_in_end[e->id].empty()) {
	// 	cout << cars_in_end[e->id].front() << endl;
	// 	cars_in_end[e->id].pop();
	// }

	return ans;
}

void optimize() {
	int prev = simulate();
	for (int it = 0; it < 1000; ++it) {
		int ans = simulate();

		if (ans < prev) {
			undo_ans();
		} else {
    		print_ans(ans_file);
			prev = ans;
			cout << ans << '\n';
		}


		int i = rand() % I;
		int mx = -1;
		for (auto e : edges_in[i]) {
			if (mx == -1 || max_queue_size[mx] < max_queue_size[e->id]) {
				mx = e->id;
			}
		}
		if (mx != -1) {
			edges[mx].off += 1;
			update_ans(&edges[mx]);
		}
	}
}

int main(int argc, char** argv) {

#ifdef LOCAL
    freopen(FILE_NAME ".in", "r", stdin);
//    freopen(FILE_NAME ".out", "w", stdout);
#endif

    if (argc != 4) {
        cout << "give 3 args please" << endl;
        exit(1);
	}

	char* input_file = argv[1];
	char* output_file = argv[2];
	ans_file = argv[3];
	
	read_input(input_file);
	read_output(output_file);

	optimize();
    
    return 0;
}
