#include <bits/stdc++.h>

using namespace std;

const int maxn = 1e5;

map <string, vector <int> > m;
map <string, int> sz;
bool b[maxn];
vector <string> g[maxn];

int main() {
	freopen("b_lovely_landscapes.txt", "r", stdin);
	freopen("b_ans", "w", stdout);
	int n;
	cin >> n;
	cerr << n << '\n';
	for (int i = 0; i < n; i++) {
		
		char c;
		cin >> c;
		int k;
		cin >> k;
		string s;
		while (k--) {
			cin >> s;
			m[s].push_back(i);
			sz[s]--;
			g[i].push_back(s);
		}
	}
	cerr << "!\n";
	cout << n << '\n';
	int o = 0;
	while (sz.begin() != sz.end()) {
		o++;
		string s = (sz.begin())->first;
		for (int i = 0; i < m[s].size(); i++) {
			int k = m[s][i];
			if (!b[k]) {
				cout << k << '\n';
				b[k] = 1;
				for (int j = 0; j < g[k].size(); j++) {
					if (sz.find(g[k][j]) != sz.end())
						sz[g[k][j]]--;
				}
			}
		}
		sz.erase(sz.begin());
		if (o % 1000 == 0)
			cerr << o << '\n';
	} 

	




	return 0;
}
