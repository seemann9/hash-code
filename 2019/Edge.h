#pragma once

class Edge {
public:
    Edge();
    Edge(int start, int end, int weight);
    bool operator < (const Edge& other) const;
    bool operator == (const Edge& other) const;
    int getStart() const;
    int getEnd() const;
private:
    int start, end;
    int weight;
};
