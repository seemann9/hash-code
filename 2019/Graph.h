#pragma once

#include <vector>
#include "Edge.h"
#include "EdgeList.h"

class Graph {
public:
    class Iterator {
    public:
        typedef ptrdiff_t difference_type;
        typedef VertexNumber value_type;
        typedef VertexNumber& reference;
        typedef VertexNumber* pointer;
        typedef std::input_iterator_tag iterator_category;

        Iterator(const Graph* graph, VertexNumber i);
        Iterator& operator++();
        Iterator operator++(int);
        VertexNumber operator*();
        bool operator!=(const Iterator& other);
    private:
        void skipDeleted();
        VertexNumber i;
        const Graph* graph;
    };

    Graph();
    Graph(int vertexCount);
    Graph(int vertexCount, std::vector<Edge> edges);
    Iterator begin();
    Iterator end();
    int getSize();
    bool containsEdge(VertexNumber u, VertexNumber v);
    void deleteVertex(VertexNumber vertex);
    bool isDeleted(VertexNumber vertex) const;
    EdgeList getNeighbors(VertexNumber vertex);
    void mergeVertices(VertexNumber u, VertexNumber v);
protected:
    int vertexCount;
    std::vector<bool> deleted;
    std::vector<std::vector<VertexNumber>> neighbors;
};
