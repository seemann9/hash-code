#pragma once
#include <cstdio>

#define TEST_PATH_FORMAT "./vc-exact-public/vc-exact_%03d.hgr"
extern FILE* logger;
#define TO_LOG(args...) fprintf(logger, args); fflush(logger)
#define TO_ERR(args...) fprintf(stderr, args)
