#include <algorithm>
#include <iterator>
#include "Macro.h"
#include "Graph.h"
#include "EdgeList.h"

EdgeList::Iterator::Iterator(const EdgeList* edgeList, std::vector<VertexNumber>::iterator it) : edgeList(edgeList), it(it) {
    skipDeleted();
}

EdgeList::Iterator& EdgeList::Iterator::operator++() {
    ++it;
    skipDeleted();
    return (*this);
}

EdgeList::Iterator EdgeList::Iterator::operator++(int) {
    Iterator temp(*this);
    ++it;
    skipDeleted();
    return temp;
}

VertexNumber EdgeList::Iterator::operator*() {
    return *it;
}

bool EdgeList::Iterator::operator!=(const Iterator& other) {
    return it != other.it;
}

void EdgeList::Iterator::skipDeleted() {
    while (it != edgeList->list->end() && edgeList->graph->isDeleted(*it)) {
        ++it;
    }
}

EdgeList::EdgeList(const Graph* graph, std::vector<VertexNumber>* list) : graph(graph), list(list) {};

int EdgeList::getSize() {
    return std::distance(begin(), end());
}

EdgeList::Iterator EdgeList::begin() {
    return Iterator(this, list->begin());
}

EdgeList::Iterator EdgeList::end() {
    return Iterator(this, list->end());
}
