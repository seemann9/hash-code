#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <ctime>
#include <cmath>
#include <string>
#include <iomanip>
#include <vector>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <cassert>
#include <queue>
#include <bitset>
//#include "Graph.h"

using namespace std;

int n;
class Photo {
public:
    std::set<std::string> tags;
//    std::bitset<500> btags;
    bool vertical, glued;
    int id, left, right;

    int getCostTo(const Photo& o) {
        //int common = (btags & o.btags).count();
        int common = 0;
        for (std::string tag : tags) {
            if (o.tags.count(tag) > 0) {
                common++;
            }
        }
        return std::min(std::min(int(tags.size() - common), int(o.tags.size() - common)), common);
    }

    Photo glueWith(const Photo& o) {
        Photo res;
        res.id = id;
        res.left = id;
        res.right = o.id;
        res.glued = true;
//        res.btags = btags | o.btags;
        res.tags = tags;
        for (std::string tag : o.tags) {
          res.tags.insert(tag);
        }
        res.vertical =false;
        return res;
    }
};

std::set<std::string> gtags;
std::map<std::string, int> tagnumber;
vector<Photo> photos;
void read(int argc, char **argv) {
    freopen(argv[1], "r", stdin);

    cin >> n;

    for (int i = 0; i < n ; ++i) {
        Photo p;
        char c;
        cin >> c;
        p.vertical = (c == 'V');
        p.id = i;
        p.glued = false;
        int numtags;
        cin >> numtags;
        for (int j = 0; j < numtags; ++j) {
            string tag;
            cin >> tag;
            p.tags.insert(tag);
            gtags.insert(tag);
        }
        photos.push_back(p);
    }
    int tnum = 0;
    for (std::string tag : gtags) {
        tagnumber[tag] = tnum++;
    }

/*    for (Photo& p : photos) {
        for (std::string tag : p.tags) {
            p.btags.set(tagnumber[tag]);
        }
    }*/
    cerr << "tags " << gtags.size() << '\n';

    fclose(stdin);
}

int main(int argc, char **argv) {
    srand(time(NULL));
    read(argc, argv);

    freopen(argv[2], "r", stdin);
    freopen(argv[3], "w", stdout);

    vector<Photo> old = photos;
    vector<Photo> inp;

    int cnt;
    cin >> cnt;
    for (int i = 0; i < cnt; ++i) {
      int a;
      cin >> a;
      if (photos[a].vertical) {
        int b;
        cin >> b;
        inp.push_back(photos[a].glueWith(photos[b]));
      } else {
        inp.push_back(photos[a]);
      }
    }

    photos = inp;

    std::vector<Photo> ans = photos;
//    random_shuffle(ans.begin(), ans.end());

    cout << ans.size() << '\n';

    vector<int> cs(ans.size() + 1, 0);

    int res = 0;
    for (int i = 0; i < ans.size(); ++i) {
        auto &p = ans[i];
        if (i > 0) {
          int ccost = p.getCostTo(ans[i - 1]);
          res += ccost;
          cs[i] = ccost;
        }
    }

    int ITER = 1e7;
    for (int it = 0; it < ITER; ++it) {
      if (it % 100000 == 0) {
        cerr << "Iter: " << it << " Res: " << res << '\n';
      }
      if (rand() % 2) {
        int i = rand() % ans.size();
        int j = i;
        while (j == i) {
          j = rand() % ans.size();
        }

        int oldc = cs[i] + cs[i + 1] + cs[j] + cs[j + 1];
        int c0 = 0, c1 = 0, c2 = 0, c3 = 0, newc = 0;
        swap(ans[i], ans[j]);
        if (i > 0) {
          c0 = ans[i - 1].getCostTo(ans[i]);
          newc += c0;
        }
        if (i + 1 < ans.size()) {
          c1 = ans[i + 1].getCostTo(ans[i]);
          newc += c1;
        }
        if (j > 0) {
          c2 = ans[j - 1].getCostTo(ans[j]);
          newc += c2;
        }
        if (j + 1 < ans.size()) {
          c3 = ans[j + 1].getCostTo(ans[j]);
          newc += c3;
        }


        if (newc > oldc) {
          res += newc - oldc;
          cerr << "Res: " << res << '\n';
          if (i > 0) {
            cs[i] = c0;
          }
          if (i + 1 < ans.size()) {
            cs[i + 1] = c1;
          }
          if (j > 0) {
            cs[j] = c2;
          }
          if (j + 1 < ans.size()) {
            cs[j + 1] = c3;
          }
        } else {
          swap(ans[i], ans[j]);
        }
      } else {
        int i = rand() % ans.size();
        while (!ans[i].glued) {
          i = rand() % ans.size();
        }
        int j = i;
        while (j == i or !ans[j].glued) {
          j = rand() % ans.size();
        }

        int oldc = cs[i] + cs[i + 1] + cs[j] + cs[j + 1];
        Photo pi, pj;
        if (rand() % 2) {
          pi = old[ans[i].left].glueWith(old[ans[j].right]);
          pj = old[ans[i].right].glueWith(old[ans[j].left]);
        } else {
          pi = old[ans[i].left].glueWith(old[ans[j].left]);
          pj = old[ans[i].right].glueWith(old[ans[j].right]);
        }

        int c0 = 0, c1 = 0, c2 = 0, c3 = 0, newc = 0;
        if (i > 0) {
          c0 = ans[i - 1].getCostTo(pi);
          newc += c0;
        }
        if (i + 1 < ans.size()) {
          c1 = ans[i + 1].getCostTo(pi);
          newc += c1;
        }
        if (j > 0) {
          c2 = ans[j - 1].getCostTo(pj);
          newc += c2;
        }
        if (j + 1 < ans.size()) {
          c3 = ans[j + 1].getCostTo(pj);
          newc += c3;
        }

        if (newc > oldc) {
          ans[i] = pi;
          ans[j] = pj;
          res += newc - oldc;
          cerr << "Res: " << res << '\n';
          if (i > 0) {
            cs[i] = c0;
          }
          if (i + 1 < ans.size()) {
            cs[i + 1] = c1;
          }
          if (j > 0) {
            cs[j] = c2;
          }
          if (j + 1 < ans.size()) {
            cs[j + 1] = c3;
          }
        }
      }
    }

    res = 0;
    for (int i = 0; i < ans.size(); ++i) {
      auto &p = ans[i];
      if (i > 0) {
        int ccost = p.getCostTo(ans[i - 1]);
        res += ccost;
        cs[i] = ccost;
      }
      if (p.glued) {
        cout << p.left << ' ' << p.right << '\n';
      } else {
        cout << p.id << '\n';
      }
    }


    cerr << "Res: " << res << '\n';
    cerr << "Tags " << gtags.size() << '\n';

    return 0;
}

