#include "Edge.h"

Edge::Edge() : start(0), end(0), weight(1) {};

Edge::Edge(int start, int end, int weight) : start(start), end(end), weight(weight) {}

bool Edge::operator < (const Edge& other) const {
    if (start != other.start) {
        return start < other.start;
    }
    return end < other.end;
}

bool Edge::operator == (const Edge& other) const {
    return start == other.start && end == other.end;
}

int Edge::getStart() const {
    return start;
}

int Edge::getEnd() const {
    return end;
}
