#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <ctime>
#include <cmath>
#include <string>
#include <iomanip>
#include <vector>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <cassert>
#include <queue>
#include <bitset>
//#include "Graph.h"

using namespace std;

class Photo {
public:
    std::set<std::string> tags;
    std::vector<int> ids;

    bool vertical;
    int id;
    int getCostTo(const Photo& o) {
        int common = 0;
        for (std::string tag : tags) {
            if (o.tags.count(tag) > 0) {
                common++;
            }
        }
        return std::min(std::min(int(tags.size() - common), int(o.tags.size() - common)), common);
    }

    void unite(const Photo &o) {
        for (int id: o.ids) {
            ids.push_back(id);
        }
        for (std::string tag: o.tags) {
            tags.insert(tag);
        }
    }
};

struct Edge {
    int cost, u, v;

    bool operator<(const Edge &e) {
        return cost > e.cost;
    }
};

struct DSU {
    std::vector<int> par;

    DSU(int n) {
        par.resize(n, -1);
        for (int i = 0; i < n; i++) {
            par[i] = i;
        }
    }

    int get(int v) {
        if (v == par[v]) {
            return v;
        }
        return get(par[v]);
    }

    int unite(int u, int v) {
        u = get(u);
        v = get(v);
        if (u == v) {
            return 0;
        }
        par[u] = v;
        return 1;
    }
};

std::set<std::string> gtags;
vector<Photo> photos;
vector<Photo> united_photos;
vector<Photo> last_v_photo;

int n;
void read() {
    cin >> n;

    for (int i = 0; i < n ; ++i) {
        Photo p;
        char c;
        cin >> c;
        p.vertical = (c == 'V');
        p.id = i;
        p.ids.push_back(i);
        int numtags;
        cin >> numtags;
        for (int j = 0; j < numtags; ++j) {
            string tag;
            cin >> tag;
            p.tags.insert(tag);
            gtags.insert(tag);
        }
        photos.push_back(p);
    }
}

std::vector<std::vector<int> > g; // final graph
std::vector<int> used;
std::vector<int> order;

void dfs(int v) {
    order.push_back(v);
    used[v] = 1;
    for (auto u: g[v]) {
        if (!used[u]) {
            dfs(u);
        }
    }
}

template <typename T>
T lexical_cast(const std::string& str)
{
    T var;
    std::istringstream iss;
    iss.str(str);
    iss >> var;
    // deal with any error bits that may have been set on the stream
    return var;
}

int build_order(const std::vector<Photo>& photos) {
    std::vector<Edge> ev;

    int edges = 0;
    for (int i = 0; i < (int)photos.size(); i++) {
        for (int j = i + 1; j < (int)photos.size(); j++) {
            Photo p = photos[i];
            Photo p2 = photos[j]; 
            if (p.getCostTo(p2) > 0) {
                edges++;
                ev.push_back(Edge{p.getCostTo(p2), i, j});
            }
        }
        if (i % 100 == 0) {
            cerr << i << ' ' << edges << '\n';
        }
    }
    sort(ev.begin(), ev.end());

    std::vector<int> ends(photos.size(), 2);
    DSU dsu(photos.size());
    g.clear();
    g.resize(photos.size(), std::vector<int>());
    int answer = 0;

    cerr << "Starting greedy algo..." << '\n';

    for (Edge e: ev) {
        int u = e.u, v = e.v;
        if (dsu.get(u) == dsu.get(v)) {
            continue;
        }
        if (ends[u] == 0 || ends[v] == 0) {
            continue;
        }
        answer += e.cost;
        cerr << e.cost << '\n';
        ends[u]--;
        ends[v]--;
        g[u].push_back(v);
        g[v].push_back(u);
        
        int u_res = dsu.unite(u, v);
        if (u_res == 0) {
            throw std::runtime_error("failed to unite vertices!");
        }
    }

    used.clear();
    used.resize(photos.size(), 0);
    order.clear();
    for (int i = 0; i < (int)photos.size(); i++) {
        if (!used[i]) {
            if (g[i].size() > 2) {
                assert(false);
            }
            if (g[i].size() == 2) {
                continue;
            }

            dfs(i);
        }
    }
    for (int i = 0; i < (int)photos.size(); i++) {
        assert(used[i]);
    }

    cerr << "Probable answer: " << answer << '\n';
    assert(order.size() == photos.size());

    for (int i = 0; i < (int)order.size(); i++) {
        for (int id: photos[order[i]].ids) {
            cout << id << ' ';
        }        
        cout << '\n';
    }

    cerr << "Edges: " << edges << '\n';

    return answer;
}

int main(int argc, char **argv) {
    //std::cout << (string)argv[0] << ' ' << (string)argv[1] << std::endl;
    int sz = lexical_cast<int>(argv[2]);
    cerr << sz << '\n';

    freopen(argv[1], "r", stdin);
    read();
    cerr << "Tags: " << gtags.size() << '\n';

    for (auto p: photos) {
        if (p.vertical) {
            if (last_v_photo.empty()) {
                last_v_photo.push_back(p);
            } else {
                last_v_photo[0].unite(p);
                united_photos.push_back(last_v_photo[0]);
                last_v_photo.pop_back();
            }
        } else {
            united_photos.push_back(p);
        }
    }

    if (!last_v_photo.empty()) {
        throw std::runtime_error("last_v_photo vector is not empty!!");
    }
    photos = united_photos;
    random_shuffle(photos.begin(), photos.end());

    cout << photos.size() << '\n';
    std::vector<Photo> cur_photos;
    int summary_answer = 0;
    for (int i = 0; i < (int)photos.size(); i += sz) {
        cerr << "Cluster begin: " << i << endl;
        cur_photos.clear();
        for (int j = 0; j < min((int)photos.size() - i, sz); j++) {
            cur_photos.push_back(photos[i + j]);
        }
        summary_answer += build_order(cur_photos);
        cerr << "Summary answer: " << summary_answer << endl;
    }


    return 0;
}

