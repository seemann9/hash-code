#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <ctime>
#include <cmath>
#include <string>
#include <iomanip>
#include <vector>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <cassert>
#include <queue>
#include <bitset>
#include <sstream>
//#include "Graph.h"

using namespace std;

int n;
class Photo {
public:
    std::set<std::string> tags;
    std::bitset<500> btags;
    bool vertical, glued;
    int id, left, right;

    int getCostTo(const Photo& o) {
        int common = (btags & o.btags).count();
        /*
        for (std::string tag : tags) {
            if (o.tags.count(tag) > 0) {
                common++;
            }
        }*/
        return std::min(std::min(int(btags.count() - common), int(o.btags.count() - common)), common);
    }
    bool operator < (const Photo& o) const {
        return tags.size() < o.tags.size();
        /*
        if (tags.size() != o.tags.size()) {
            return tags.size() < o.tags.size();
        }
        return btags < o.btags;*/
    }

    Photo glueWith(const Photo& o) {
        Photo res;
        res.id = id;
        res.left = id;
        res.right = o.id;
        res.glued = true;
        res.btags = btags | o.btags;
        res.vertical =false;
        return res;
    }
};

std::set<std::string> gtags;
std::map<std::string, int> tagnumber;
vector<Photo> photos;
void read() {
    cin >> n;

    for (int i = 0; i < n ; ++i) {
        Photo p;
        char c;
        cin >> c;
        p.vertical = (c == 'V');
        p.id = i;
        p.glued = false;
        int numtags;
        cin >> numtags;
        for (int j = 0; j < numtags; ++j) {
            string tag;
            cin >> tag;
            p.tags.insert(tag);
                gtags.insert(tag);
        }
        photos.push_back(p);
    }
    int tnum = 0;
    for (std::string tag : gtags) {
        tagnumber[tag] = tnum++;
    }

    for (Photo& p : photos) {
        for (std::string tag : p.tags) {
            p.btags.set(tagnumber[tag]);
        }
    }
    cerr << "tags " << gtags.size() << '\n';
}

struct Edge {
    int cost, u, v;

    bool operator<(const Edge &e) const {
        if (cost != e.cost) {
            return cost > e.cost;
        } else  if (u != e.u) {
            return u < e.u;
        }
        return v < e.v;
    }
};

struct DSU {
    std::vector<int> par;

    DSU(int n) {
        par.resize(n, -1);
        for (int i = 0; i < n; i++) {
            par[i] = i;
        }
    }

    int get(int v) {
        if (v == par[v]) {
            return v;
        }
        return get(par[v]);
    }

    int unite(int u, int v) {
        u = get(u);
        v = get(v);
        if (u == v) {
            return 0;
        }
        par[u] = v;
        return 1;
    }
};

std::vector<std::vector<int> > g; // final graph
std::vector<int> used;
std::vector<int> order;

void dfs(int v) {
    order.push_back(v);
    used[v] = 1;
    for (auto u: g[v]) {
        if (!used[u]) {
            dfs(u);
        }
    }
}

template <typename T>
T lexical_cast(const std::string& str)
{
    T var;
    std::istringstream iss;
    iss.str(str);
    iss >> var;
    // deal with any error bits that may have been set on the stream
    return var;
}

const int MAXEDGES = 8e7;

std::vector<Photo> build_order(const std::vector<Photo>& photos) {
    std::vector<int> ends(photos.size(), 2);
    DSU dsu(photos.size());
    g.clear();
    g.resize(photos.size(), std::vector<int>());
    int answer = 0;

    int iter = 0;
    while (true) {
        iter++;
        cerr << "Start iter " << iter << '\n';
        std::vector<Edge> ev;
        std::set<Edge> setEdges;
        std::vector<std::vector<Edge>> edgesByCost;
        
        int edges = 0;
        int lowestCost = 0;

        for (int i = 0; i < (int)photos.size(); i++) {
            if (ends[i] == 0) {
                continue;
            }
            int dsui = dsu.get(i);
            for (int j = i + 1; j < (int)photos.size(); j++) {
                if (ends[j] == 0 || dsui == dsu.get(j)) {
                    continue;
                }
                Photo p = photos[i];
                Photo p2 = photos[j]; 
                edges++;
                Edge ed = Edge{p.getCostTo(p2), i, j};
                while (edgesByCost.size() <= ed.cost) {
                    edgesByCost.push_back(std::vector<Edge>());
                }
                edgesByCost[ed.cost].push_back(ed);
                lowestCost = std::min(lowestCost, ed.cost);
                if (edges > MAXEDGES) {
                    edges--;
                    while (lowestCost < edgesByCost.size() && edgesByCost[lowestCost].size() == 0) {
                        lowestCost++;
                    }
                    if (lowestCost < edgesByCost.size()) {
                        edgesByCost[lowestCost].pop_back();
                    }
//                    setEdges.erase(*setEdges.rbegin());
                }
            }
            if (i % 100 == 0) {
                cerr << "Vertex number " << i << "Edges " << edges <<  '\n'; 
            }
        }

        if (edges == 0) {
            break;
        }

        cerr << "Found edges: " << edges << '\n';

        for (int curCost = edgesByCost.size() - 1; curCost >= 0; --curCost) {
            for (Edge e: edgesByCost[curCost]) {
                int u = e.u, v = e.v;
                if (dsu.get(u) == dsu.get(v)) {
                    continue;
                }
                if (ends[u] == 0 || ends[v] == 0) {
                    continue;
                }
                answer += e.cost;
                cerr << e.cost << '\n';
                ends[u]--;
                ends[v]--;
                g[u].push_back(v);
                g[v].push_back(u);
                
                int u_res = dsu.unite(u, v);
                if (u_res == 0) {
                    throw std::runtime_error("failed to unite vertices!");
                }
            }
        }
    }

    used.clear();
    used.resize(photos.size(), 0);
    order.clear();
    for (int i = 0; i < (int)photos.size(); i++) {
        if (!used[i]) {
            if (g[i].size() > 2) {
                assert(false);
            }
            if (g[i].size() == 2) {
                continue;
            }

            dfs(i);
        }
    }
    for (int i = 0; i < (int)photos.size(); i++) {
        assert(used[i]);
    }

    vector<Photo> ans;
    for (int i : order) {
        ans.push_back(photos[i]);
    }

    cerr << "Cost " << answer << '\n';

    return ans;
}

int main() {
    srand(time(NULL));
    read();
    int edges = 0;
    /*
    for (Photo p : photos) {
        for (Photo p2 : photos) {
            if (p.id < p2.id) {
                if (p.getCostTo(p2) > 0) {
                    edges++;
                }
            }
        }
        if (p.id % 100 == 0) {
            cerr << p.id << ' ' << edges << ' ' << gtags.size() << '\n';
        }
    }*/

    /*
    std::vector<Photo> verticalPhotos;
    std::vector<Photo> gluedPhotos;
    std::vector<Photo> horizontalPhotos;
    for (Photo p : photos) {
        if (p.vertical) {
            verticalPhotos.push_back(p);
        } else {
            horizontalPhotos.push_back(p);
        }
    }
    random_shuffle(verticalPhotos.begin(), verticalPhotos.end());
    sort(verticalPhotos.begin(), verticalPhotos.end());
    for (int i = 0; i < verticalPhotos.size() / 2; ++i) {
    //    cerr << i << ' ' << verticalPhotos.size() - i - 1 << '\n';
        gluedPhotos.push_back(verticalPhotos[i].glueWith(verticalPhotos[verticalPhotos.size() - i - 1]));
    }

    if (gluedPhotos.size() > 1) {
        cerr << "Start better pairs\n";
        const int ITER = 1e7;

        for (int it = 0; it < ITER; ++it) {
            int i = rand() % gluedPhotos.size();

            int j;
            do {
                j = rand() % gluedPhotos.size();
            } while (i == j);
            
            Photo pi, pj;
            if (rand() % 2) {
              pi = photos[gluedPhotos[i].left].glueWith(photos[gluedPhotos[j].right]);
              pj = photos[gluedPhotos[i].right].glueWith(photos[gluedPhotos[j].left]);
            } else {
              pi = photos[gluedPhotos[i].left].glueWith(photos[gluedPhotos[j].left]);
              pj = photos[gluedPhotos[i].right].glueWith(photos[gluedPhotos[j].right]);
            }
            if (pi.btags.count() + pj.btags.count() > gluedPhotos[i].btags.count() + gluedPhotos[j].btags.count()) {
                gluedPhotos[i] = pi;
                gluedPhotos[j] = pj;
            }
        }
        cerr << "End better pairs\n";
    }


    for (Photo p : horizontalPhotos) {
        gluedPhotos.push_back(p);
    }

    cerr << "glued: " << gluedPhotos.size() << '\n';

    photos = gluedPhotos;
*/
    
    vector<bool> used(n);
    Photo p = photos[rand() % photos.size()];
    used[p.id] = true;
    int cntUsed = 1;
    std::vector<Photo> ans;
    ans.push_back(p);
    int ansCost = 0;
    std::map<int, int> edgeCosts;
    while (true) {
        if (ans.size() % 100 == 0) {
            cerr << "Iter " << ans.size() << '\n';
        }
//        cerr << "Iter: " << p.id << '\n';
        int bestIndex = -1;
        int bestSize = -1;
        for (int i = 0; i < photos.size(); ++i) {
            Photo& p2 = photos[i];
            if (used[p2.id]) {
                continue;
            }
            int dist = p.getCostTo(p2);
//            cerr << "Try " << p2.id << ' ' << dist << '\n';
            if (dist > bestSize) {
                bestSize = dist;
                bestIndex = i;
            }
        }
        if (bestIndex == -1) {
            break;
        }
        used[photos[bestIndex].id] = true;
        cntUsed++;
        if (photos[bestIndex].vertical) {
            int bestIndex2 = -1;
            int bestSize2 = -1;
            for (int i = 0; i < photos.size(); ++i) {
                Photo& p2 = photos[i];
                if (used[p2.id] || !p2.vertical) {
                    continue;
                }
                int dist = p.getCostTo(p2.glueWith(photos[bestIndex]));
    //            cerr << "Try " << p2.id << ' ' << dist << '\n';
                if (dist > bestSize2) {
                    bestSize2 = dist;
                    bestIndex2 = i;
                }
            }

            if (bestIndex2 == -1) {
                break;
            }
            used[photos[bestIndex2].id] = true;
            cntUsed++;
            ans.push_back(photos[bestIndex].glueWith(photos[bestIndex2]));
            p = photos[bestIndex];
            ansCost += bestSize2;
        } else {
            ans.push_back(photos[bestIndex]);
            p = photos[bestIndex];
            ansCost += bestSize;
        }
    }
    
//    vector<Photo> ans = build_order(photos);

    cout << ans.size() << '\n';
    for (auto p : ans) {
        if (p.glued) {
            cout << p.left << ' ' << p.right << '\n';
        } else {
            cout << p.id << '\n';
        }
    }

//    cerr << "Edges: " << edges << '\n';
//    cerr << "Tags " << gtags.size() << '\n';


    /*
    for (auto e : edgeCosts) {
        cerr << e.first << ' ' << e.second << '\n';
    }*/

    cerr << "Cost " << ansCost << '\n';
    
    return 0;
}

