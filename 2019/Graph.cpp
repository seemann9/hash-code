#include <cassert>
#include <algorithm>
#include "Macro.h"
#include "Graph.h"

Graph::Iterator::Iterator(const Graph* graph, VertexNumber i) : graph(graph), i(i) {
    skipDeleted();
}

Graph::Iterator& Graph::Iterator::operator++() {
    if (i < graph->vertexCount) {
        ++i;
    }
    skipDeleted();
    return (*this);
}

Graph::Iterator Graph::Iterator::operator++(int) {
    Iterator temp(*this);
    if (i < graph->vertexCount) {
        ++i;
    }
    skipDeleted();
    return temp;
}

VertexNumber Graph::Iterator::operator*() {
    return i;
}

bool Graph::Iterator::operator!=(const Iterator& other) {
    return i != other.i;
}

void Graph::Iterator::skipDeleted() {
    while (i < graph->vertexCount && graph->deleted[i]) {
        ++i;
    }
}

Graph::Graph() : vertexCount(0) {};

Graph::Graph(int vertexCount) : vertexCount(vertexCount) {
    deleted.resize(vertexCount, false);
    neighbors.resize(vertexCount);
}

Graph::Graph(int vertexCount, std::vector<Edge> edges) : Graph(vertexCount) {
    for (const Edge& edge : edges) {
        neighbors[edge.getStart()].push_back(edge.getEnd());
        neighbors[edge.getEnd()].push_back(edge.getStart());
    }

    for (VertexNumber vertex : (*this)) {
        std::sort(neighbors[vertex].begin(), neighbors[vertex].end());
    }
}

Graph::Iterator Graph::begin() {
    return Iterator(this, 0);
}

Graph::Iterator Graph::end() {
    return Iterator(this, vertexCount);
}

int Graph::getSize() {
    return std::distance(begin(), end());
}

bool Graph::containsEdge(VertexNumber u, VertexNumber v) {
    auto it = std::lower_bound(neighbors[u].begin(), neighbors[u].end(), v);
    return it != neighbors[u].end() && *it == v;
}

void Graph::deleteVertex(VertexNumber vertex) {
    deleted[vertex] = true;
}

bool Graph::isDeleted(VertexNumber vertex) const {
    return deleted[vertex];
}

EdgeList Graph::getNeighbors(VertexNumber vertex) {
    return EdgeList(this, &neighbors[vertex]);
}

void Graph::mergeVertices(VertexNumber u, VertexNumber v) {
    deleteVertex(u);
    deleteVertex(v);

    std::vector<VertexNumber> mergedNeighbors;
    mergedNeighbors.insert(mergedNeighbors.end(), getNeighbors(u).begin(), getNeighbors(u).end());
    mergedNeighbors.insert(mergedNeighbors.end(), getNeighbors(v).begin(), getNeighbors(v).end());
    std::sort(mergedNeighbors.begin(), mergedNeighbors.end());
    auto it = std::unique(mergedNeighbors.begin(), mergedNeighbors.end());
    mergedNeighbors.resize(std::distance(mergedNeighbors.begin(), it));

    neighbors.emplace_back(std::move(mergedNeighbors));
    deleted.push_back(false);
    VertexNumber mergedVertex = vertexCount;
    vertexCount++;
    for (VertexNumber neighbor : neighbors[mergedVertex]) {
        neighbors[neighbor].push_back(mergedVertex);
    }
}

