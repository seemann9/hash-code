#pragma once

#include <vector>

using VertexNumber = int;

class Graph;

class EdgeList {
public:
    class Iterator {
    public:
        typedef ptrdiff_t difference_type;
        typedef VertexNumber value_type;
        typedef VertexNumber& reference;
        typedef VertexNumber* pointer;
        typedef std::input_iterator_tag iterator_category;

        Iterator(const EdgeList* edgeList, std::vector<VertexNumber>::iterator it);
        Iterator& operator++();
        Iterator operator++(int);
        VertexNumber operator*();
        bool operator!=(const Iterator& other);
    private:
        void skipDeleted();
        std::vector<VertexNumber>::iterator it;
        const EdgeList* edgeList;
    };

    EdgeList(const Graph* graph, std::vector<VertexNumber>* list);
    int getSize();
    Iterator begin();
    Iterator end();
protected:
    const Graph* graph;
    std::vector<VertexNumber>* list;
};
